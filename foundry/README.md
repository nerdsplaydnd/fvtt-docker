# fvtt Dockerfile
Foundry VTT Dockerfile and container build environment with documentation


#### Use this git repository to download or build a Node.js Docker container for foundryvtt

- TODO(joemartin) Add summary and links to sections of README
- TODO(joemartin) Add helper script to repo and document below


#### Get started by setting up your local development environment

Pull this Git repository to your development host
```
git clone https://gitlab.com/nerdsplaydnd/fvtt-docker.git
```

Change directory into the git repository you just checked out
```
cd ./fvtt-docker/
```

Set FOUNDRY environment variables in your local environment
```
export ENV FOUNDRY_VERSION=10.291
export ENV FOUNDRY_PATH=$(pwd)
export ENV FOUNDRY_DATA_DIR="fvttdata"
export ENV FOUNDRY_DATA=${FOUNDRY_PATH}/${FOUNDRY_DATA_DIR}
export ENV DOCKER_TAG="${FOUNDRY_DATA_DIR}"
export ENV GITLAB_PRIVATE_TOKEN=""
```

List currently set environment variables which contain the string 'FOUNDRY'
```
env|grep FOUNDRY
```


#### Download your Foundry content and create a new git branch

Git clone your fvttdata repository
```
git clone https://gitlab.com/nerdsplaydnd/${FOUNDRY_DATA_DIR}.git
```

Make a new branch of your git repository to track your development changes
```
git checkout -b ${FOUNDRY_DATA_DIR}/`NEW BRANCH NAME`
```


#### Run a local development instance of Foundry Virtual Table Top server

Authenticate to the container registry with `docker login` for a prompt to enter your GitLab username and password
```
docker login registry.gitlab.com
```

Download an fvtt image pre-built from this Dockerfile from the GitLab container registry
```
docker pull registry.gitlab.com/nerdsplaydnd/fvtt:latest
```

Run the Docker image you just pulled as a local foundryvtt instance to develop, test, or play game content
```
docker run -dt -p 30000:30000 -v ${FOUNDRY_DATA}:${FOUNDRY_DATA} registry.gitlab.com/nerdsplaydnd/fvtt:latest
```


#### Download a version of the foundryvtt install files to `./fvtt/` in this build directory.

Pull a copy of the foundryvtt package from the GitLab package registry
```
curl --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" https://gitlab.com/api/v4/projects/29158674/packages/generic/foundryvtt/${FOUNDRY_VERSION}/foundryvtt-${FOUNDRY_VERSION}.zip --output ${FOUNDRY_PATH}/foundryvtt-${FOUNDRY_VERSION}.zip
```

Unzip the foundryvtt package into your docker fvtt build environment
```
unzip ./foundryvtt*.zip -d ${FOUNDRY_PATH}/fvtt/
```

#### Alternative: Download the foundryvtt installation package source

Pull the foundryvtt package source code from GitLab git registry 
```
git clone https://gitlab.com/nerdsplaydnd/fvtt.git ${FOUNDRY_PATH}/fvtt/
```

Pull the foundryvtt binary from GitLab LFS
```
curl --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" https://gitlab.com/api/v4/projects/29158674/packages/generic/foundryvtt/${FOUNDRY_VERSION}/foundryvtt --output ${FOUNDRY_PATH}/fvtt/foundryvtt
```


#### Build a new container image from the Dockerfile in this repository

Build an fvtt image locally by using `docker build` in the directory containing the Dockerfile.
```
docker build -t fvtt:local .
```

Run the Docker image you just built as a local foundryvtt instance to develop, test, or play game content
```
docker run -dt -p 30000:30000 -v ${FOUNDRY_DATA}:${FOUNDRY_PATH}/fvttdata fvtt:local
```

Tag and push your local docker image to the GitLab registry using `docker tag` and `docker push`
```
docker tag fvtt:local registry.gitlab.com/nerdsplaydnd/fvtt:${DOCKER_TAG}
docker push registry.gitlab.com/nerdsplaydnd/fvtt:${DOCKER_TAG}
```

Run your fvtt container image anywhere, pull from registry.gitlab.com/nerdsplaydnd/fvtt
```
docker run -dt -p 30000:30000 -v ${FOUNDRY_DATA}:${FOUNDRY_PATH}/fvttdata registry.gitlab.com/nerdsplaydnd/fvtt:${DOCKER_TAG}
```

Push your new fvtt container image to the registry
```
docker push registry.gitlab.com/nerdsplaydnd/fvtt:${DOCKER_TAG}
```


#### Alternative: Build a new development container image based upon Ubuntu

Pull the latest, upstream Ubuntu Docker image for fvtt development
```
docker run -it -p 30000:30000 -v ${FOUNDRY_PATH}/fvtt:${FOUNDRY_PATH}/fvtt -v ${FOUNDRY_DATA}:${FOUNDRY_PATH}/fvttdata ubuntu:latest bash
```

Install Foundry software dependencies on Ubuntu
```
apt update && apt upgrade -y
apt install curl npm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
source ~/.bashrc
nvm install v14.17.5
```

Run Foundry VTT inside your local Ubuntu container
```
node /var/foundryvtt/fvtt/resources/app/main.js --dataPath=/var/foundryvtt/fvttdata
```

Gracefully detach from a container and leave it running using the `CTRL-p CTRL-q` key sequence.

copy the `CONTAINER ID` from the list of running containers
```
docker container ls
```

Save your running container locally by commiting it as a new image 
```
docker commit `CONTAINER ID` registry.gitlab.com/nerdsplaydnd/fvtt/ubuntu:${DOCKER_TAG}
```

Run the Ubuntu container image you just built as a local foundryvtt instance to develop, test, or play game content
```
docker run -dt -p 30000:30000 -v ${FOUNDRY_PATH}/fvtt:${FOUNDRY_PATH}/fvtt -v ${FOUNDRY_DATA}:${FOUNDRY_PATH}/fvttdata registry.gitlab.com/nerdsplaydnd/fvtt/ubuntu:${DOCKER_TAG} node ${FOUNDRY_PATH}/fvtt/resources/app/main.js --dataPath=${FOUNDRY_DATA}
```

Push your new Ubuntu container image to the registry
```
docker push registry.gitlab.com/nerdsplaydnd/fvtt/ubuntu:${DOCKER_TAG}
```


#### Alternative: Run Foundry VTT locally as a Node.js application

Run foundryvtt Node.js application dirrectly on your localhost
```
node ${FOUNDRY_PATH}/fvtt/resources/app/main.js --dataPath=${FOUNDRY_DATA}
```


#### Visit your local Foundry VTT server in your browser at [http://localhost:30000](http://localhost:30000)


#### Alternative: Visit your local Foundry VTT server in your browser at [http://local.nerdsplaydnd.com](http://local.nerdsplaydnd.com)

Optional: Edit your /etc/hosts file 
```
127.0.0.1  local.nerdsplaydnd.com
```

Optional: Run nginx proxy container to route traffic from port 80 to 30000
- TODO(joemartin) Document nginx proxy steps
```
docker run -dt -p 80:80 -p 443:443 -v nginx:nginx nginx:latest
```


#### Backup your Foundry data often and SAVE your development progress

Use `git push` command to back up your fvttdata directory to the git server
```
git add `PATH TO NEW OR MODIFIED FILES`
git commit -m " `A COMMIT MESSAGE ABOUT YOUR CHANGES` "
git push --set-upstream origin ${FOUNDRY_DATA_DIR}/`NEW BRANCH NAME`
```

Visit the GitLab URL output in your terminal to create a merge request to have your branch and your progress merged to the main branch.
```
  Have a beer and wait for your code to be reviewed ...
```

