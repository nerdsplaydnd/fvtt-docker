# Debian Dockerfile and build directory  
D&D DevOps' Debian-based, general purpose, GNU/Linux, Docker image  

### Run upstream Debian container

* `docker login`  
* `docker run -itv $(pwd):/dnddevops debian:stable-slim`  

### Pull and run modified, private Debian container

* `docker login registry.gitlab.com`
* `docker pull registry.gitlab.com/nerdsplaydnd/fvtt/dnddevops:stable`  
* `docker run -itv $(pwd):/dnddevops registry.gitlab.com/nerdsplaydnd/fvtt/dnddevops:stable`  

### Build, run and push new private Debian container

* `docker build -t dnddevops:stable .`  
* `docker run -itv $(pwd):/dnddevops dnddevops:stable`  
* `docker tag dnddevops:stable registry.gitlab.com/nerdsplaydnd/fvtt/dnddevops:stable`  
* `docker push regibuild -t dnddevops:stable .`nerdsplaydnd/fvtt/dnddevops:stable`  

