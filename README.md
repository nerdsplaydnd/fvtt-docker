# Nerds Play D&D Docker Build Repository

Each subdirectory in this repository is a Docker build context with a Dockerfile and any additional content required to build the container image described in the individual README and Dockerfile of that build context.  

**Handy links for getting started with Docker**

### Download Docker Desktop  

https://www.docker.com/products/docker-desktop  

### Docker Documentation and Quick Start Guide  

https://docs.docker.com/  

### Browse Public Docker Hub Images   

https://hub.docker.com/  

### Nerds Play D&D Private Docker Image Registry  

https://gitlab.com/nerdsplaydnd/fvtt/container_registry  

### Dockerfile Best Practices Guidelines

https://docs.docker.com/develop/develop-images/dockerfile_best-practices/  

### README Syntax and Styleguide Documentation for Markdown files  

https://www.markdownguide.org/basic-syntax/  

